# OpenAPI document to HTTP Handler

A Go library to make it easier to convert an OpenAPI specification from kin-openapi to an `http.HandlerFunc` which presents an API documentation site.

## Usage

First, download the project:

```sh
go get gitlab.com/jamietanna/openapi-doc-http-handler
```

Then, depending on the flavour of API documentation, check out the module-specific documentation, through the [Go docs](https://pkg.go.dev/gitlab.com/jamietanna/openapi-doc-http-handler).

### Modules

Currently supported API documentation:

- Stoplight Elements (`elements` package)

## License

This code is licensed under the Apache 2 License.
